FROM alpine:3.8
MAINTAINER Christopher Pitts

RUN apk add --no-cache texlive-full biber &&  \
	ln -s /usr/bin/luatex /usr/bin/lualatex && \
	luaotfload-tool -u
